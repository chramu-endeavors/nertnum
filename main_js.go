package main

import (
	"fmt"
	"math/big"
	"syscall/js"
	"time"
)

func main() {
	fmt.Println("Main start")
	defer fmt.Println("Main end")
	document.Call("getElementById", "input-decimal").Call("addEventListener", "input", js.FuncOf(onInputDecimal))
	select {}
}

var document = global.Get("document")

// called synchronously in JS event loop, so no worry of clobbering closured vars
var onInputDecimal = func() func(js.Value, []js.Value) interface{} {
	inputBigInt := &big.Int{}
	isComputing := false
	cancelChan := make(chan struct{}, 10)
	elOutputInert := document.Call("getElementById", "output-inert")
	return func(this js.Value, args []js.Value) interface{} {
		_, ok := inputBigInt.SetString(args[0].Get("target").Get("value").String(), 10)
		if !ok || inputBigInt.Sign() < 0 {
			return true
		}
		if isComputing {
			cancelChan <- struct{}{}
		}
		go func() {
			var outputInert string
			defer func() {
				isComputing = false
				panicVal := recover()
				if panicVal == nil {
					elOutputInert.Set("innerHTML", outputInert)
				} else if panicVal != "cancelled" {
					elOutputInert.Set("innerHTML", panicVal)
				}
			}()
			// clear out anything left in cancelChan, which is probably nothing,
			// but I think due to a race with isComputing it may contain something
		clearCancelChanLoop:
			for {
				select {
				case <-cancelChan:
				default:
					break clearCancelChanLoop
				}
			}
			isComputing = true
			outputInert = inertify(inputBigInt, cancelChan, time.Now())
		}()
		return true
	}
}()

var global = js.Global()
