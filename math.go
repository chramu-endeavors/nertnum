package main

import (
	"fmt"
	"math/big"
	"runtime"
	"strings"
	"time"
)

// returns an inert string representation of n
func inertify(n *big.Int, cancelChan chan struct{}, startTime time.Time) string {
	if n.Sign() == 0 {
		return "0"
	}
	res := make([]string, 1, 3)
	res[0] = "("
	chunkSize := 0
	chunkVal := ""
	chunkSizeBig := &big.Int{}
	for _, exponent := range primeFactorize(n, cancelChan, startTime) {
		subnert := inertify(exponent, cancelChan, startTime)
		if subnert == chunkVal {
			chunkSize++
			continue
		}
		if chunkSize > 0 {
			chunkSizeNert := inertify(chunkSizeBig.SetUint64(uint64(chunkSize)), cancelChan, startTime)
			if len(chunkVal)+1+len(chunkSizeNert) < len(chunkVal)*chunkSize {
				res = append(res, chunkVal, ":", chunkSizeNert)
			} else {
				res = append(res, strings.Repeat(chunkVal, chunkSize))
			}
		}
		chunkVal = subnert
		chunkSize = 1
	}
	if chunkSize > 0 {
		chunkSizeNert := inertify(chunkSizeBig.SetUint64(uint64(chunkSize)), cancelChan, startTime)
		if len(chunkVal)+1+len(chunkSizeNert) < len(chunkVal)*chunkSize {
			res = append(res, chunkVal, ":", chunkSizeNert)
		} else {
			res = append(res, strings.Repeat(chunkVal, chunkSize))
		}
	}
	res = append(res, ")")
	return strings.Join(res, "")
}

var bigOne = (&big.Int{}).SetUint64(1)

// returns a slice of prime exponents in increasing order of prime bases
func primeFactorize(n *big.Int, cancelChan chan struct{}, startTime time.Time) []*big.Int {
	n = (&big.Int{}).Set(n)
	res := make([]*big.Int, 0, 10)
	if n.Sign() <= 0 {
		fmt.Printf("Error: tried to factorize negative integer %s\n", n.Text(10))
		return res
	}
	if n.Cmp(bigOne) == 0 {
		return res
	}
	primeCand := &big.Int{}
	quo, rem := &big.Int{}, &big.Int{}
	tempBig := &big.Int{}
	wheelTurn, wheelCur := &big.Int{}, &big.Int{}
	for wheelTurn.SetUint64(0); true; wheelTurn.Add(wheelTurn, bigOne) {
		if time.Now().Sub(startTime) > 1*time.Second {
			panic("took too long; gave up")
		}
		runtime.Gosched() // doesn't seem to do anything
		select {          // doesn't seem to do anything either
		case <-cancelChan:
			panic("cancelled")
		default:
		}
		for wheelCur.SetUint64(0); wheelCur.Cmp(wheelTurnSizeBig) < 0; wheelCur.Add(wheelCur, bigOne) {
			primeCand.Mul(wheelTurn, wheelTurnSizeBig).Add(primeCand, tempBig.SetUint64(uint64(primeWheel[wheelCur.Uint64()])))
			if primeCand.Cmp(bigOne) == 0 { // the very first wheel element is always 1
				continue
			}
			quo.QuoRem(n, primeCand, rem)
			if rem.Sign() != 0 || !isPrime(primeCand, cancelChan, startTime) {
				continue
			}
			exponent := uint64(0)
			for rem.Sign() == 0 {
				exponent++
				n.Set(quo)
				quo.QuoRem(n, primeCand, rem)
			}
			numPrimesLTE := countPrimesLTE(primeCand, cancelChan, startTime)
			newExponents := make([]*big.Int, numPrimesLTE-uint64(len(res)))
			for i := 0; i < len(newExponents); i++ {
				newExponents[i] = &big.Int{}
			}
			newExponents[len(newExponents)-1].SetUint64(exponent)
			res = append(res, newExponents...)
			if n.Cmp(bigOne) == 0 {
				return res
			}
		}
	}
	return res // should never be reached, but the compiler complains otherwise
}

var bigMaxUint64 = (&big.Int{}).SetUint64((1 << 64) - 1)

func isPrime(n *big.Int, cancelChan chan struct{}, startTime time.Time) bool {
	if !n.ProbablyPrime(1) {
		return false
	}
	// according to the Go docs, "ProbablyPrime is 100% accurate for inputs less than 2^64."
	if n.Cmp(bigMaxUint64) <= 0 {
		return true
	}
	panic("isPrime(n) not implemented for n >= 2^64")
}

// returns number of primes less than or equal to n
func countPrimesLTE(n *big.Int, cancelChan chan struct{}, startTime time.Time) uint64 {
	res := uint64(0)
	primeCand := &big.Int{}
	tempBig := &big.Int{}
	wheelTurn, wheelCur := &big.Int{}, &big.Int{}
	for wheelTurn.SetUint64(0); true; wheelTurn.Add(wheelTurn, bigOne) {
		if time.Now().Sub(startTime) > 1*time.Second {
			panic("took too long; gave up")
		}
		runtime.Gosched() // doesn't seem to do anything
		select {          // doesn't seem to do anything either
		case <-cancelChan:
			panic("cancelled")
		default:
		}
		for wheelCur.SetUint64(0); wheelCur.Cmp(wheelTurnSizeBig) < 0; wheelCur.Add(wheelCur, bigOne) {
			primeCand.Mul(wheelTurn, wheelTurnSizeBig).Add(primeCand, tempBig.SetUint64(uint64(primeWheel[wheelCur.Uint64()])))
			if primeCand.Cmp(n) > 0 {
				return res
			}
			if isPrime(primeCand, cancelChan, startTime) {
				res++
			}
		}
	}
	return res
}

var maxWheelSeedPrime = 13
var wheelTurnSize = 1

// list of possibly-prime offsets from multiples of the turn size
var primeWheel = func() []int {
	wheelSeedSieve := make([]bool, maxWheelSeedPrime+1)
	numWheelSeeds := 0
	for i := 2; i < len(wheelSeedSieve); i++ {
		if wheelSeedSieve[i] {
			continue
		}
		numWheelSeeds++
		wheelTurnSize *= i
		for j := i * 2; j < len(wheelSeedSieve); j += i {
			wheelSeedSieve[j] = true
		}
	}
	wheelSeeds := make([]int, numWheelSeeds)
	iWheelSeed := 0
	for i := 2; i < len(wheelSeedSieve); i++ {
		if !wheelSeedSieve[i] {
			wheelSeeds[iWheelSeed] = i
			iWheelSeed++
		}
	}
	wheelSieve := make([]bool, wheelTurnSize)
	numSeedCoprimes := wheelTurnSize
	for _, seed := range wheelSeeds {
		for i := 0; i < len(wheelSieve); i += seed {
			if wheelSieve[i] {
				continue
			}
			wheelSieve[i] = true
			numSeedCoprimes--
		}
	}
	wheel := make([]int, numSeedCoprimes)
	iWheel := 0
	for i := 0; i < len(wheelSieve); i++ {
		if !wheelSieve[i] {
			wheel[iWheel] = i
			iWheel++
		}
	}
	return wheel
}()
var wheelTurnSizeUint64 = uint64(wheelTurnSize)
var wheelTurnSizeBig = (&big.Int{}).SetUint64(wheelTurnSizeUint64)
var primeWheelBig = func() []*big.Int {
	wheel := make([]*big.Int, len(primeWheel))
	for i, v := range primeWheel {
		wheel[i] = (&big.Int{}).SetUint64(uint64(v))
	}
	return wheel
}()
